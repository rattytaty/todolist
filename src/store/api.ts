import axios from "axios";

export type backendTodolist = {
    id: string
    title: string
}
export type TaskType = {
    id: string
    title: string
    description: string
    deadline: string
    completed: boolean
}
export type DataForCreationTask = {
    title: string
    description: string
    deadline: string
}
export type DataForUpdateTask = {
    title?: string
    description?: string
    deadline?: string
    completed?: boolean
}

export const instance = axios.create({
    baseURL: "https://64492e4eb88a78a8f00022cf.mockapi.io/"
})
export const todolistsApi = {
    getTodolists() {
        return instance.get<backendTodolist[]>("todolists")
    },
    createTodolist(title: string) {
        return instance.post<backendTodolist>("todolists", {title})
    },
    deleteTodolist(todolistId: string) {
        return instance.delete<backendTodolist>(`todolists/${todolistId}`)
    },
    updateTodolist(todolistId: string, data: { title: string }) {
        return instance.put(`todolists/${todolistId}`, data)
    },
}
export const tasksApi = {
    getTasks(todolistId: string) {
        return instance.get<TaskType[]>(`todolists/${todolistId}/tasks`)
    },
    createTask(todolistId: string, data: DataForCreationTask) {
        return instance.post<TaskType>(`todolists/${todolistId}/tasks`, data)
    },
    deleteTask(todolistId: string, taskId: string) {
        return instance.delete<TaskType>(`todolists/${todolistId}/tasks/${taskId}`)
    },
    updateTask(todolistId: string, taskId: string, data: DataForCreationTask) {
        return instance.put<TaskType>(`todolists/${todolistId}/tasks/${taskId}`, data)
    }

}





