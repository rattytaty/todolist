
//types
export type setLoadingAction = ReturnType<typeof setLoadingAC>
export type setErrorAction = ReturnType<typeof setErrorAC>
type ActionTypes = setLoadingAction|setErrorAction
type appState={
    loading:boolean
    error:string
}

//actions
export const setLoadingAC = (loading:boolean)=>{
    return {type:"APP-LOADING", loading} as const
}
export const setErrorAC = (error:string)=>{
    return {type:"APP-ERROR", error} as const
}

const initialState:appState = {
    loading: false,
    error:""
}
export const appReducer = (state: appState = initialState, action: ActionTypes): appState =>{
switch (action.type) {
    case "APP-LOADING":
        return {...state, loading:action.loading}
    case "APP-ERROR":
        return {...state, error:action.error}
    default:
        return state
}

}