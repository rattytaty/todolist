import {backendTodolist, todolistsApi} from "../api";
import {Dispatch} from "redux";
import {setErrorAC, setErrorAction, setLoadingAC, setLoadingAction} from "./app-reducer";
import {AxiosError} from "axios";

//types
type  ActionTypes =
    SetTodolistsAction
    | CreateTodolistAction
    | DeleteTodolistAction
    | ChangeTodolistFilter
    | setLoadingAction
    | setErrorAction
export type CreateTodolistAction = ReturnType<typeof createTodolistAC>
export type DeleteTodolistAction = ReturnType<typeof deleteTodolistAC>
export type SetTodolistsAction = ReturnType<typeof setTodolistsAC>
export type ChangeTodolistFilter = ReturnType<typeof changeTodolistFilterAC>
export type frontendTodolist = {
    id: string
    title: string
    filter: filterValues
}
export type filterValues = "All" | "Active" | "Completed"

//Thunks
export const fetchTodolistsTC = () => (dispatch: Dispatch<ActionTypes>) => {
    dispatch(setLoadingAC(true))
    todolistsApi.getTodolists()
        .then((response) => {
            dispatch(setTodolistsAC(response.data))
            dispatch(setLoadingAC(false))
        })
        .catch((error: AxiosError) => {
            dispatch(setLoadingAC(false))
            dispatch(setErrorAC(error.message))
        })
}
export const createTodolistTC = (todoTitle: string) =>  (dispatch: Dispatch<ActionTypes>) => {
    dispatch(setLoadingAC(true))
    todolistsApi.createTodolist(todoTitle)
        .then((response)=>{
            dispatch(createTodolistAC(response.data))
            dispatch(setLoadingAC(false))
        })
        .catch((error: AxiosError) => {
            dispatch(setLoadingAC(false))
            dispatch(setErrorAC(error.message))
        })
}
export const deleteTodolistTC = (todolistId: string) => (dispatch: Dispatch<ActionTypes>) => {
    dispatch(setLoadingAC(true))
    todolistsApi.deleteTodolist(todolistId)
        .then((response)=>{
            dispatch(deleteTodolistAC(todolistId))
            dispatch(setLoadingAC(false))
        })
        .catch((error: AxiosError)=>{
            dispatch(setErrorAC(error.message))
            dispatch(setLoadingAC(false))
        })
}

//actions
export const setTodolistsAC = (todolists: Array<backendTodolist>) => {
    return {type: "SET-TODOLISTS", todolists} as const
}
export const createTodolistAC = (newTodolist: backendTodolist) => ({type: "CREATE-TODOLIST", newTodolist} as const)
export const deleteTodolistAC = (todolistId: string) => ({type: "DELETE-TODOLIST", todolistId} as const)
export const changeTodolistFilterAC = (todolistId: string, filterValue: filterValues) => {
    return {type: "CHANGE-FILTER", todolistId, filterValue} as const
}

export const todolistsReducer = (state: Array<frontendTodolist> = [], action: ActionTypes): Array<frontendTodolist> => {
    switch (action.type) {
        case "SET-TODOLISTS":
            return action.todolists.map((todolist) => {
                return {...todolist, filter: "All"}
            })
        case "CREATE-TODOLIST":
            return [...state, {...action.newTodolist, filter: "All"}]
        case "DELETE-TODOLIST":
            return state.filter((tl) => tl.id !== action.todolistId)
        case "CHANGE-FILTER":
            return state.map((todolist) => todolist.id === action.todolistId ? {
                ...todolist,
                filter: action.filterValue
            } : todolist)
        default:
            return state
    }


}