import {DataForUpdateTask, tasksApi, TaskType} from "../api";
import {Dispatch} from "redux";

import {AppRootStateType} from "../Store";
import {dataFromFormik} from "../../components/Todolist/AddNewTask";
import {createTodolistAC, deleteTodolistAC} from "./todolists-reducer";
import {setErrorAC, setErrorAction, setLoadingAC, setLoadingAction} from "./app-reducer";
import {AxiosError} from "axios";


//types
type  ActionTypes =
    setTasksAction
    | CreateTodolistAction
    | DeleteTodolistAction
    | addTaskAction
    | deleteTaskAction
    | updateTaskAction
    | setLoadingAction
    | setErrorAction
export type setTasksAction = ReturnType<typeof setTasksAC>
export type addTaskAction = ReturnType<typeof addTaskAC>
export type deleteTaskAction = ReturnType<typeof deleteTaskAC>
export type updateTaskAction = ReturnType<typeof updateTaskAC>
export type CreateTodolistAction = ReturnType<typeof createTodolistAC>
export type DeleteTodolistAction = ReturnType<typeof deleteTodolistAC>


export type TasksArrays = { [todolistId: string]: Array<TaskType> }

//thunks
export const getTasksTC = (todolistId: string) => (dispatch: Dispatch<ActionTypes>) => {
    dispatch(setLoadingAC(true))
    tasksApi.getTasks(todolistId)
        .then((response) => {
                dispatch(setTasksAC(response.data, todolistId))
                dispatch(setLoadingAC(false))
            }
        )
        .catch((error: AxiosError) => {
            dispatch(setErrorAC(error.code ? error.code : ""))
            dispatch(setLoadingAC(false))
        })
}
export const createTaskTC = (todolistId: string, data: dataFromFormik) => (dispatch: Dispatch<ActionTypes>) => {
    dispatch(setLoadingAC(true))
    const {title, description, date, time} = data
    tasksApi.createTask(todolistId, {title, description, deadline: `${date}T${time}`})
        .then((response) => {
            dispatch(addTaskAC(response.data, todolistId))
            dispatch(setLoadingAC(false))
        })
        .catch((error: AxiosError) => {
            dispatch(setErrorAC(error.message))
            dispatch(setLoadingAC(false))
        })
}
export const deleteTaskTC = (todolistId: string, taskId: string) => (dispatch: Dispatch<ActionTypes>) => {
    dispatch(setLoadingAC(true))
    tasksApi.deleteTask(todolistId, taskId)
        .then((response) => {
            dispatch(deleteTaskAC(todolistId, taskId))
            dispatch(setLoadingAC(false))
        })
        .catch((error: AxiosError) => {
            dispatch(setErrorAC(error.message))
            dispatch(setLoadingAC(false))
        })


}
export const updateTaskTC = (todolistId: string, taskId: string, data: DataForUpdateTask) => (dispatch: Dispatch<ActionTypes>, getState: () => AppRootStateType) => {
    dispatch(setLoadingAC(true))
    const taskForUpdate = getState().tasks[todolistId].find((task) => task.id === taskId)
    if (!taskForUpdate) {
        console.warn("task not found")
        return
    }
    const model: TaskType = {...taskForUpdate, ...data}
    tasksApi.updateTask(todolistId, taskId, model)
        .then((response) => {
            dispatch(setLoadingAC(false))
            dispatch(updateTaskAC(todolistId, taskId, response.data))
        })
        .catch((error: AxiosError) => {
            dispatch(setErrorAC(error.message))
            dispatch(setLoadingAC(false))
        })
}

//actions
export const setTasksAC = (tasks: Array<TaskType>, todolistId: string) => {
    return {type: "SET-TASKS", tasks, todoId: todolistId} as const
}
export const addTaskAC = (task: TaskType, todolistId: string) => {
    return {type: "ADD-TASK", task, todolistId} as const
}
export const deleteTaskAC = (todolistId: string, taskId: string) => {
    return {type: "DELETE-TASK", todolistId, taskId} as const
}
export const updateTaskAC = (todolistId: string, taskId: string, task: TaskType) => {
    return {type: "UPDATE-TASK", todolistId, taskId, task} as const
}

export const tasksReducer = (state: TasksArrays = {}, action: ActionTypes): TasksArrays => {
    switch (action.type) {
        case "SET-TASKS":
            return {...state, [action.todoId]: [...action.tasks]}
        case "CREATE-TODOLIST":
            return {...state, [action.newTodolist.id]: [],}
        case "DELETE-TODOLIST":
            const copyState = {...state}
            delete copyState[action.todolistId]
            return copyState
        case "ADD-TASK":
            return {...state, [action.todolistId]: [...state[action.todolistId], action.task]}
        case "DELETE-TASK":
            return {...state, [action.todolistId]: state[action.todolistId].filter((task) => task.id !== action.taskId)}
        case "UPDATE-TASK":
            return {
                ...state,
                [action.todolistId]: state[action.todolistId].map((task) => task.id === action.taskId ? {...action.task} : task)
            }
        default:
            return state
    }

}