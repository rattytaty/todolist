import React from 'react';
import {useAppDispatch, useAppSelector} from "../store/Store";
import {setErrorAC} from "../store/reducers/app-reducer";

export const LoadingCircle = React.memo(() => {

    const {error, loading} = useAppSelector(state => state.app)
    const dispatch = useAppDispatch()

    const onErrorButton = () => {
        dispatch(setErrorAC(""))
    }
    return (<
        div className="m-2 absolute text-right text-black text-4xl   fixed  z-50   top-0 right-0">

        {error
            ? <div>
                <div className="badge m-2  text-4xl h-12 badge-error gap-2">{error}</div>
                <button onClick={onErrorButton} className="btn btn-error">ok</button>
            </div>
            : <></>}
        {loading
            ? <div> Loading...
                <div
                    className="inline-block h-12 w-12 animate-spin rounded-full border-4 border-solid border-current border-r-transparent align-[-0.125em] motion-reduce:animate-[spin_1.5s_linear_infinite]"
                    role="status">
                </div>
            </div>
            : <></>
        }

    </div>)

})
