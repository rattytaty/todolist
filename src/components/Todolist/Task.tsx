import React, {ChangeEvent, useState} from "react";
import {useAppSelector} from "../../store/Store";
import {EditTaskForm} from "./EditTask";

export type TaskProps = {
    todolistId: string
    description: string
    deadline: string
    id: string
    completed: boolean
    title: string
    changeCompletedStatus: (taskId: string, taskCompletedStatus: boolean) => void
    deleteTask: (taskId: string) => void
}


export const Task: React.FC<TaskProps> = React.memo((props) => {

    const loading = useAppSelector(state => state.app.loading)

    const changeCompletedStatus = (e: ChangeEvent<HTMLInputElement>) => {
        props.changeCompletedStatus(props.id, e.currentTarget.checked)
    }
    const deleteTask = () => {
        props.deleteTask(props.id)
    }

    const [date, time] = props.deadline.split("T")
    const convertedDate = date.split("-").reverse().join("-").replaceAll("-", ".")
    const convertedTime = time.slice(0, 5)
    const [editMode, setEditMode] = useState(false)

    return <div key={props.id} className={" rounded-md  text-black  bg-blue-50 m-1 w-72  relative"}>
        <div title={"Task title"} className={"m-1 text-black text-center text-xl"}>{props.title}</div>
        <div title={"Task description"} className={
            "m-1 text-gray-600 w-72 break-all"
        }>{props.description}</div>
        <div title={"Task deadline"} className={"m-1 text-gray-600"}>Complete before:</div>
        <div title={"Task deadline"} className={"m-1 text-gray-600"}>{
            `${convertedDate} ${convertedTime}`
        }</div>

        <EditTaskForm description={props.description} title={props.title} isPopUpActive={editMode}
                      setIsPopUpActive={setEditMode} todolistId={props.todolistId} taskId={props.id}/>


        <button disabled={loading} onClick={() => {
            setEditMode(true)
        }} className="btn m-1 btn-xs btn-primary">Edit
        </button>
        <button disabled={loading} title={"Delete task"} className="mr-3 absolute right-0 top-0"
                onClick={deleteTask}>x
        </button>
        <div className="m-1.5 form-control absolute right-0 bottom-0">
            <input disabled={loading} title={"Change status"} type="checkbox" checked={props.completed}
                   onChange={changeCompletedStatus} className="checkbox checkbox-sm checkbox-success"/>
        </div>
    </div>

})