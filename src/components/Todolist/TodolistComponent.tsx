import React, {ChangeEvent, useCallback, useEffect, useState} from "react";
import {useAppDispatch, useAppSelector} from "../../store/Store";
import {getTasksTC} from "../../store/reducers/tasks-reducer";
import {Task} from "./Task";
import {filterValues} from "../../store/reducers/todolists-reducer";
import {TaskType} from "../../store/api";
import {AddNewTaskForm} from "./AddNewTask";

export type TodolistProps = {
    todolistTitle: string
    filter: filterValues
    id: string
    tasks: Array<TaskType>
    deleteTodolist: (todolistId: string) => void

    deleteTask: (todolistId: string, taskId: string) => void
    changeCompletedStatusTask: (todolistId: string, taskId: string, taskCompletedStatus: boolean) => void
    changeFilter: (todolistId: string, filterValue: filterValues) => void

}


export const TodolistComponent: React.FC<TodolistProps> = React.memo((props) => {
    console.log("TodolistComponent")
    const dispatch = useAppDispatch()
    useEffect(() => {
        dispatch(getTasksTC(props.id))
    }, [props.id, dispatch])
    const loading = useAppSelector(state => state.app.loading)

    const deleteTodolist = () => {
        props.deleteTodolist(props.id)
    }

    const deleteTask = useCallback((taskId: string) => {
        props.deleteTask(props.id, taskId)
    }, [props.id])
    const changeCompletedStatus = useCallback((taskId: string, taskCompletedStatus: boolean) => {
        props.changeCompletedStatusTask(props.id, taskId, taskCompletedStatus)
    }, [props.id])
    const changeFilter = (filterValue: filterValues) => () => {
        props.changeFilter(props.id, filterValue)
    }
    const [searchValue, setSearchValue] = useState<string>("")
    const onChangeSearchValue = (event: ChangeEvent<HTMLInputElement>) => {
        setSearchValue(event.currentTarget.value)
    }


    const sortedTasks =
        props.tasks && props.tasks.filter(({completed}) => {
            if (props.filter === "Active") return completed === false;
            else if (props.filter === "Completed") return completed === true;
            return true;
        })
            .filter((task) => task.title.toLowerCase().includes(searchValue.toLowerCase()))


    const [isPopUpActive, setIsPopUpActive] = useState<boolean>(false)


    return <div
        className={"m-2.5 rounded-md bg-gray-200  float-left relative  overflow-x-scroll w-76"}>
        <AddNewTaskForm todolistId={props.id} isPopUpActive={isPopUpActive} setIsPopUpActive={setIsPopUpActive}/>

        <div title={"Todolist title"}
             className={"m-2 text-black text-center text-3xl font-semibold"}>{props.todolistTitle}</div>


        <div className={"flex justify-center"}>
            <button disabled={loading} onClick={() => {
                setIsPopUpActive(true)
            }} className="btn btn-outline btn-primary w-60 btn-sm ">Add a new Task
            </button>
        </div>


        <button disabled={loading} className="btn btn-outline btn-circle btn-sm absolute right-0 top-0
         m-1" onClick={deleteTodolist}>
            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                 className="inline-block w-4 h-4 stroke-current ">
                <path className={"text-black"} strokeLinecap="round" strokeLinejoin="round" strokeWidth="2"
                      d="M6 18L18 6M6 6l12 12"></path>
            </svg>
        </button>


        <div className={"text-black text-center text-2xl  font-medium"}>TASKS:</div>
        <div title={"Change filter"}
             className="m-1 flex justify-center btn-group btn-group-vertical lg:btn-group-horizontal  ">
            <button disabled={loading}
                onClick={changeFilter("All")}
                    className={`btn btn-sm btn-ghost ${props.filter === "All" ? "btn-active" : ""}`}>All
            </button>
            <button disabled={loading}
                onClick={changeFilter("Active")}
                    className={`btn btn-sm btn-ghost ${props.filter === "Active" ? "btn-active" : ""}`}>Active
            </button>
            <button disabled={loading}
                onClick={changeFilter("Completed")}
                    className={`btn btn-sm btn-ghost ${props.filter === "Completed" ? "btn-active" : ""}`}>Completed
            </button>
        </div>
        <div className={"flex justify-center"}><input disabled={loading} type="text" placeholder={"Search by task title..."}
                    className=" bg-blue-50 input input-bordered input-sm m-1 w-72 " value={searchValue}
                    onChange={onChangeSearchValue}/></div>

        {sortedTasks && sortedTasks.map((t) =>
            <Task key={t.id}
                  id={t.id}
                  title={t.title}
                  completed={t.completed}
                  changeCompletedStatus={changeCompletedStatus}
                  deleteTask={deleteTask}
                  description={t.description}
                  deadline={t.deadline}
                  todolistId={props.id}
            />)}

    </div>
})
