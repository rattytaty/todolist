import {useAppDispatch, useAppSelector} from "../store/Store";
import React, {useCallback, useEffect} from "react";
import {
    changeTodolistFilterAC,
    createTodolistTC,
    deleteTodolistTC,
    fetchTodolistsTC,
    filterValues
} from "../store/reducers/todolists-reducer";
import {deleteTaskTC, updateTaskTC} from "../store/reducers/tasks-reducer";
import {AddItemForm} from "../components/AddItemForm";
import {TodolistComponent} from "../components/Todolist/TodolistComponent";

export const TodolistsPage: React.FC = React.memo(() => {


    const dispatch = useAppDispatch()
    const todolists = useAppSelector(state => state.todolists)
    const tasks = useAppSelector(state => state.tasks)

    const deleteTodolist = useCallback((todolistId: string) => {
        dispatch(deleteTodolistTC(todolistId))
    }, [dispatch])
    const addTodolist = useCallback((todolistTitle: string) => {
        dispatch(createTodolistTC(todolistTitle))
    }, [dispatch])
    const changeFilter = useCallback((todolistId: string, filterValue: filterValues) => {
        dispatch(changeTodolistFilterAC(todolistId, filterValue))
    }, [dispatch])

    const deleteTask = useCallback((todolistId: string, taskId: string) => {
        dispatch(deleteTaskTC(todolistId, taskId))
    }, [dispatch])
    const changeCompletedStatusTask = useCallback((todolistId: string, taskId: string, completedStatus: boolean) => {

        dispatch(updateTaskTC(todolistId, taskId, {completed: completedStatus}))
    }, [dispatch])

    useEffect(() => {

        dispatch(fetchTodolistsTC())

    }, [dispatch])

    return <div className={"bg-blue-300 min-h-screen"}>
        <AddItemForm addItem={addTodolist}/>
        <div className={" table-cell overflow-y-scroll "}>
            {todolists && todolists.map((todolist) =>
                <TodolistComponent key={todolist.id}
                                   id={todolist.id}
                                   filter={todolist.filter}
                                   todolistTitle={todolist.title}
                                   tasks={tasks[todolist.id]}

                                   deleteTodolist={deleteTodolist}
                                   changeFilter={changeFilter}
                                   deleteTask={deleteTask}
                                   changeCompletedStatusTask={changeCompletedStatusTask}
                />
            )}</div>
    </div>
})